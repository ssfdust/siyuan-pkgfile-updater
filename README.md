## Usage

```bash
RUST_LOG=info cargo run -- <template_file> <output_file>
```

### example
```bash
RUST_LOG=info cargo run -- ./templates/PKGBUILD.template /tmp/PKGBUILD
```
