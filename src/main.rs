//! # SiYuan PKGBUILD file updater
//!
//! This is a tool to update PKGBUILD file for SiYuan. It will update the
//! version number and sha512sums automatically. And then it will call makepkg
//! up regenerate the SRCINFO file for AUR.
#[macro_use]
extern crate log;

mod duma;

use ring::digest;
use std::env;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Write, Read};

use duma::download::http_download;
use reqwest::header::USER_AGENT;

// FIREFOX User Agent string.
const FIREFOX_UA: &str = "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0";
const SIYUAN_API_ADDR: &str = "https://api.github.com/repos/siyuan-note/siyuan/releases/latest";
const FILENAME: &str = "siyuan-lastest.tar.gz";

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();

    env_logger::builder().filter_level(log::LevelFilter::Info).init();

    // 获取命令行参数
    if args.len() < 3 {
        eprintln!("Usage: ./program <template_file> <output_file>");
        return Ok(());
    }
    let template_file_path = &args[1];
    let output_file_path = &args[2];
    fetch_latest_version(SIYUAN_API_ADDR).and_then(|pkgver| {
        let siyuan_download_url = format!("https://github.com/siyuan-note/siyuan/releases/download/v{pkgver}/siyuan-{pkgver}-linux.tar.gz");
        // let siyuan_download_url = format!("https://release.b3log.org/siyuan/siyuan-2.10.7-linux.tar.gz");
        info!("Download url: {}", siyuan_download_url);
        sha512sum_url(&siyuan_download_url).and_then(|sha512sum| {
            let template_file = File::open(template_file_path)?;
            let output_file = File::create(output_file_path)?;

            // 读取模板文件的内容
            let reader = BufReader::new(template_file);
            let mut lines = Vec::new();
            for line in reader.lines() {
                lines.push(line?);
            }

            // 替换模板中的特定字符串
            let replaced_lines: Vec<String> = lines
                .iter()
                .map(|line| {
                    let new_line = line.replace("%pkgver%", &pkgver);
                    new_line.replace("%sha512sum%", sha512sum.as_str())
                })
                .collect();

            // 将替换后的内容写入输出文件
            let mut writer = io::BufWriter::new(output_file);
            for line in replaced_lines {
                writeln!(writer, "{}", line)?;
            }

            println!("PKGBUILD file created successfully.");
            Ok(())
        }).ok();
        Ok(())
    }).ok();
    Ok(())
}

fn sha512sum_url(url: &str) -> Result<String, Box<dyn std::error::Error>> {
    let sha512sum = http_download(url.try_into().unwrap(), FILENAME, FIREFOX_UA)
        .and_then(|_| {
            let sha512sum = File::open(FILENAME)
                .and_then(|mut f| {
                    let mut buffer = Vec::new();
                    f.read_to_end(&mut buffer)?;
                    let context = digest::digest(&digest::SHA512, &buffer);
                    let hash = context
                        .as_ref()
                        .iter()
                        .map(|x| format!("{:02x}", x))
                        .collect::<String>();
                    Ok(hash)
                })
                .unwrap_or_default();
            Ok(sha512sum)
        })
        .ok()
        .unwrap_or_default();
    Ok(sha512sum)
}

/// Fetch the latest version number from the github api.
fn fetch_latest_version(api_addr: &str) -> Result<String, Box<dyn std::error::Error>> {
    let mut version_start = false;
    let mut version = String::new();
    let client = reqwest::blocking::Client::new();
    let release_info = client
        .get(api_addr)
        .header(USER_AGENT, FIREFOX_UA)
        .send()
        .and_then(|x| x.text())
        .unwrap_or_default();

    // Parse the json string.
    let json: serde_json::Value = serde_json::from_str(&release_info)?;
    // Get the version number.
    let tag_name = json["tag_name"].as_str().unwrap_or_default();
    for i in tag_name.chars() {
        if i.is_digit(10) || i == '.' {
            version_start = true;
        }
        if version_start {
            version.push(i);
        }
    }
    // Return the version number.
    Ok(version.into())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fetch_latest_version() {
        env_logger::init();
        let version = fetch_latest_version(SIYUAN_API_ADDR).unwrap();
        assert!(version.chars().all(|x| x.is_digit(10) || x == '.'));
    }
}
